import "./App.css";
import React, { useState, useEffect } from "react";
import {
  Layout,
  Row,
  Col,
  InputNumber,
  Slider,
  Button,
  Radio,
  Checkbox,
} from "antd";
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Area,
  Line,
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import { Resizable } from "re-resizable";
import IRBracketPresets from "./IRBracketsPresets.json";
import currentTotalData from "./currentTotalData.json";

const { Header, Content } = Layout;

function App() {
  const inflation = [
    { year: 2020, rate: 0.5 },
    { year: 2019, rate: 1.1 },
    { year: 2018, rate: 1.8 },
    { year: 2017, rate: 1.0 },
    { year: 2016, rate: 0.2 },
    { year: 2015, rate: 0.0 },
    { year: 2014, rate: 0.5 },
    { year: 2013, rate: 0.9 },
    { year: 2012, rate: 2.0 },
    { year: 2011, rate: 2.1 },
    { year: 2010, rate: 1.5 },
    { year: 2009, rate: 0.1 },
  ];

  const CSGBracketPresets = [
    {
      name: 2020,
      values: [
        { bracket: 1, floor: 0, rate: 9.2 },
        { bracket: 2, floor: 15600, rate: 9.2 },
      ],
    },
  ];

  const CRDSBracketPresets = [{ name: 2020, rate: 0.5 }];

  const lfiIRBrackets = [
    { bracket: 1, floor: 0, rate: 2 },
    { bracket: 2, floor: 200 * 12, rate: 3 },
    { bracket: 3, floor: 500 * 12, rate: 4 },
    { bracket: 4, floor: 800 * 12, rate: 5 },
    { bracket: 5, floor: 1300 * 12, rate: 8 },
    { bracket: 6, floor: 1500 * 12, rate: 12 },
    { bracket: 7, floor: 1800 * 12, rate: 14 },
    { bracket: 8, floor: 2200 * 12, rate: 20 },
    { bracket: 9, floor: 3000 * 12, rate: 26 },
    { bracket: 10, floor: 4000 * 12, rate: 28 },
    { bracket: 11, floor: 4500 * 12, rate: 30 },
    { bracket: 12, floor: 5500 * 12, rate: 40 },
    { bracket: 13, floor: 10000 * 12, rate: 50 },
    { bracket: 14, floor: 25000 * 12, rate: 60 },
    { bracket: 15, floor: 33000 * 12, rate: 90 },
  ];

  const lfiCSGBrackets = {
    name: "lfi",
    values: [
      { bracket: 1, rate: 1, floor: 0 },
      { bracket: 2, rate: 2, floor: 15600 },
      { bracket: 3, rate: 3, floor: 21600 },
      { bracket: 4, rate: 8, floor: 36000 },
      { bracket: 5, rate: 12, floor: 120000 },
    ],
  };

  let marks = {};

  IRBracketPresets.forEach((preset) => {
    marks[preset.name] = {
      style: {
        color: "white",
      },
      label: preset.name,
    };
  });

  const defaultName = 2020;

  const defaultIRBrackets = IRBracketPresets.filter(
    (brackets) => brackets.name === defaultName
  )[0].values;

  const defaultCSGBrackets = CSGBracketPresets.filter(
    (brackets) => brackets.name === defaultName
  )[0];

  const defaultCRDSBrackets = CRDSBracketPresets.filter(
    (brackets) => brackets.name === defaultName
  )[0].rate;

  const [IRBrackets, setIRBrackets] = useState(defaultIRBrackets);
  const [CSGBrackets, setCSGBrackets] = useState(defaultCSGBrackets);
  const [CRDSBrackets, setCRDSBrackets] = useState(defaultCRDSBrackets);

  const getIRAmount = (revenue) => {
    let amount = 0;
    IRBrackets.forEach((bracket, i) => {
      if (revenue > bracket.floor) {
        if (IRBrackets[i + 1]) {
          if (revenue < IRBrackets[i + 1].floor) {
            amount += (revenue - bracket.floor) * (bracket.rate / 100);
          } else {
            amount +=
              (IRBrackets[i + 1].floor - bracket.floor) * (bracket.rate / 100);
          }
        } else {
          amount += (revenue - bracket.floor) * (bracket.rate / 100);
        }
      }
    });
    return amount;
  };

  const getIRRate = (revenue) => {
    return getIRAmount(revenue) / revenue;
  };

  const getCSGAmount = (revenue) => {
    let amount = 0;
    CSGBrackets.values.forEach((bracket, i) => {
      if (revenue > bracket.floor) {
        if (CSGBrackets.values[i + 1]) {
          if (revenue < CSGBrackets.values[i + 1].floor) {
            amount += (revenue - bracket.floor) * (bracket.rate / 100);
          } else {
            amount +=
              (CSGBrackets.values[i + 1].floor - bracket.floor) *
              (bracket.rate / 100);
          }
        } else {
          amount += (revenue - bracket.floor) * (bracket.rate / 100);
        }
      }
    });
    return amount;
  };

  const getCSGRate = (revenue) => {
    return getCSGAmount(revenue) / revenue;
  };

  const getCRDSRate = () => {
    return CRDSBrackets / 100;
  };

  const getCRDSAmount = (revenue) => {
    return revenue * getCRDSRate(revenue);
  };

  const [autoRange, setAutoRange] = useState(true);

  const [rangeEnd, setRangeEnd] = useState(
    (IRBrackets[IRBrackets.length - 1].floor / 10000).toFixed(0) * 20000
  );

  const range = (start, stop, step = 1) =>
    Array(Math.ceil((stop - start) / step))
      .fill(start)
      .map((x, y) => x + y * step);

  const [revenues, setRevenues] = useState(range(1000, rangeEnd, 1000));

  const [IRTotalRevenue, setIRTotalRevenue] = useState(0);
  const [CSGTotalRevenue, setCSGTotalRevenue] = useState(0);
  const [CRDSTotalRevenue, setCRDSTotalRevenue] = useState(0);
  const [TotalRevenue, setTotalRevenue] = useState(0);

  const [maxRate, setMaxRate] = useState(60);

  const curve1 = (revenue) => {
    return Math.min(revenue / 300000 + 0.03, 0.75);
  };

  const [showIR, setShowIR] = useState(true);
  const [showCSG, setShowCSG] = useState(true);
  const [showCRDS, setShowCRDS] = useState(true);

  const [displayedData, setDisplayedData] = useState(
    revenues.map((revenue) => ({
      revenue,
      IRRate: showIR ? (getIRRate(revenue) * 100).toFixed(1) : undefined,
      CSGRate: showCSG ? (getCSGRate(revenue) * 100).toFixed(1) : undefined,
      CRDSRate: showCRDS ? (getCRDSRate(revenue) * 100).toFixed(1) : undefined,
      totalRate: (getIRRate(revenue) + getCSGRate(revenue) * 100).toFixed(1),
    }))
  );

  const updateDisplayedData = () => {
    setDisplayedData(
      revenues.map((revenue) => ({
        revenue,
        IRRate: showIR ? (getIRRate(revenue) * 100).toFixed(1) : undefined,
        CSGRate: showCSG ? (getCSGRate(revenue) * 100).toFixed(1) : undefined,
        CRDSRate: showCRDS
          ? (getCRDSRate(revenue) * 100).toFixed(1)
          : undefined,
        totalRate: (
          (getIRRate(revenue) + getCSGRate(revenue) + getCRDSRate(revenue)) *
          100
        ).toFixed(1),
      }))
    );
  };

  useEffect(() => {
    updateDisplayedData();
  }, [
    rangeEnd,
    setRangeEnd,
    IRBrackets,
    setIRBrackets,
    showIR,
    showCSG,
    showCRDS,
    revenues,
    setRevenues,
  ]);

  useEffect(() => {
    setIRTotalRevenue(
      distributionData
        .map(
          (x, i) =>
            (distributionData[i].distribution *
              Number(displayedData[i].IRRate)) /
            100
        )
        .reduce((x, y) => x + y)
    );
  }, [displayedData]);

  const A = 1000;
  const m = 200;
  const c = 20000;
  const levy = (x) =>
    A * ((1 / (x - m) ** (2 / 3)) * Math.exp(-c / (2 * (x - m))));

  const levyDistributionData = revenues.map((revenue) => ({
    revenue,
    distribution: `${levy(revenue) ? levy(revenue) : 0}`,
  }));

  const distributionAcc = (x) =>
    49927.38 + (1142.874 - 49927.38) / (1 + (x / 27892.37) ** 2.610432);

  const distributionAccData = revenues.map((revenue) => ({
    revenue,
    distribution: distributionAcc(revenue),
  }));

  const distributionData = revenues.map((revenue, i) => ({
    revenue,
    distribution:
      distributionAcc(revenues[i + 1]) - distributionAcc(revenue)
        ? distributionAcc(revenues[i + 1]) - distributionAcc(revenue)
        : 0,
  }));

  const defaultIRTotalRevenue = distributionData
    .map(
      (x, i) =>
        (distributionData[i].distribution * Number(displayedData[i].IRRate)) /
        100
    )
    .reduce((x, y) => x + y);

  return (
    <div className="App">
      <Layout style={{ height: "100vh" }}>
        <Header className="header">
          <h1 style={{ textAlign: "center", color: "white", fontSize: "2em" }}>
            Impôt sur le revenu
          </h1>
        </Header>
        <div style={{ backgroundColor: "rgb(0, 21, 41)", height: "5em" }}>
          <Slider
            style={{ margin: "1em 4em" }}
            min={1980}
            max={2020}
            step={null}
            marks={marks}
            defaultValue={defaultName}
            onChange={(val) => {
              let newBrackets = IRBracketPresets.filter(
                (bracket) => bracket.name === val
              )[0].values;
              setIRBrackets(newBrackets);
              setRevenues(range(1000, rangeEnd, 1000));
              updateDisplayedData();
            }}
          ></Slider>
        </div>
        <div
          style={{
            backgroundColor: "rgb(0, 21, 41)",
            height: "5em",
          }}
        >
          <Button
            style={{ margin: "1em 4em" }}
            onClick={() => {
              setIRBrackets(lfiIRBrackets);
              setCSGBrackets(lfiCSGBrackets);
              setRangeEnd(
                (IRBrackets[IRBrackets.length - 1].floor / 10000).toFixed(0) *
                  20000
              );
              setRevenues(range(1000, rangeEnd, 1000));
              updateDisplayedData();
            }}
          >
            Proposition LFI
          </Button>
          <Button
            style={{ margin: "1em 4em" }}
            onClick={() => {
              setRevenues(range(1000, rangeEnd, 1000));
              updateDisplayedData();
            }}
          >
            Courbe 1
          </Button>
        </div>
        <Layout>
          <Layout style={{ margin: "1em" }}>
            <div
              style={{
                width: "100%",
                height: "22em",
                display: "flex",
                alignItems: "flex-end",
                overflow: "hidden",
              }}
            >
              {IRBrackets.map((bracket, i) => (
                <>
                  <div
                    style={{
                      width: "6em",
                      textAlign: "center",
                    }}
                  >
                    <Resizable
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        border: "solid 1px purple",
                        margin: "2px",
                        background: "purple",
                      }}
                      defaultSize={{
                        height: bracket.rate * 2,
                      }}
                      size={{
                        height: bracket.rate * 2,
                      }}
                      minHeight="0"
                      onResize={(e, dir, ref, d) => {
                        let newBrackets = IRBrackets;
                        newBrackets[i] = {
                          ...newBrackets[i],
                          rate: (
                            (ref.clientHeight /
                              ref.parentElement.parentElement.clientHeight) *
                            100 *
                            1.55
                          ).toFixed(0),
                        };
                        setIRBrackets(newBrackets);
                        setRevenues(range(1000, rangeEnd, 1000));
                        updateDisplayedData();
                      }}
                    >
                      <p
                        style={{ position: "relative", top: "-3.5em" }}
                      >{`Tranche ${bracket.bracket}`}</p>
                      <p
                        style={{
                          position: "relative",
                          top: "-3.5em",
                          fontSize: "1.5em",
                        }}
                      >{`${bracket.rate} %`}</p>
                    </Resizable>
                    {IRBrackets[i] !== undefined && (
                      <>
                        <div>Seuil</div>
                        <InputNumber
                          min={0}
                          max={1000000}
                          defaultValue={IRBrackets[i].floor}
                          value={IRBrackets[i].floor}
                          formatter={(value) =>
                            `${value.toLocaleString("fr")} €`
                          }
                          onChange={(val) => {
                            let newBrackets = IRBrackets;
                            newBrackets[i].floor = val;
                            setIRBrackets([...newBrackets]);
                            updateDisplayedData();
                          }}
                        />
                      </>
                    )}
                  </div>
                </>
              ))}
              <div style={{ height: "20em" }}>
                <Button
                  type="primary"
                  shape="circle"
                  icon={<MinusOutlined />}
                  size="large"
                  style={{
                    top: "40%",
                    left: "2.2em",
                    border: "2px solid purple",
                    backgroundColor: "transparent",
                    color: "purple",
                  }}
                  onClick={() => {
                    let newBrackets = IRBrackets;
                    newBrackets.pop();
                    setIRBrackets([...newBrackets]);
                    if (autoRange) {
                      setRangeEnd(
                        (
                          IRBrackets[IRBrackets.length - 1].floor / 10000
                        ).toFixed(0) * 20000
                      );
                    }
                    setRevenues(range(1000, rangeEnd, 1000));
                    updateDisplayedData();
                  }}
                />
                <Button
                  type="primary"
                  shape="circle"
                  icon={<PlusOutlined />}
                  size="large"
                  style={{
                    top: "60%",
                    border: "2px solid purple",
                    backgroundColor: "transparent",
                    color: "purple",
                  }}
                  onClick={() => {
                    setIRBrackets([
                      ...IRBrackets,
                      {
                        bracket: IRBrackets.length + 1,
                        rate: (IRBrackets[IRBrackets.length - 1].rate >= 90
                          ? 100
                          : IRBrackets[IRBrackets.length - 1].rate * 1.1
                        ).toFixed(0),
                        floor: (
                          IRBrackets[IRBrackets.length - 1].floor * 1.1
                        ).toFixed(0),
                      },
                    ]);
                    if (autoRange) {
                      setRangeEnd(
                        (
                          IRBrackets[IRBrackets.length - 1].floor / 10000
                        ).toFixed(0) * 20000
                      );
                    }
                    setRevenues(range(1000, rangeEnd, 1000));
                    updateDisplayedData();
                  }}
                />
              </div>
            </div>
          </Layout>
          <Layout style={{ margin: "1em" }}>
            <div
              style={{
                width: "100%",
                height: "22em",
                display: "flex",
                alignItems: "flex-end",
                overflow: "hidden",
              }}
            >
              {CSGBrackets.values.map((bracket, i) => (
                <>
                  <div
                    style={{
                      width: "6em",
                      textAlign: "center",
                    }}
                  >
                    <Resizable
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        border: "solid 1px blue",
                        margin: "2px",
                        background: "blue",
                      }}
                      defaultSize={{
                        height: bracket.rate * 2,
                      }}
                      size={{
                        height: bracket.rate * 2,
                      }}
                      minHeight="0"
                      onResize={(e, dir, ref, d) => {
                        let newBrackets = CSGBrackets.values;
                        newBrackets[i] = {
                          ...newBrackets[i],
                          rate: (
                            (ref.clientHeight /
                              ref.parentElement.parentElement.clientHeight) *
                            100 *
                            1.55
                          ).toFixed(0),
                        };
                        setCSGBrackets({ ...CSGBrackets, values: newBrackets });
                        setRevenues(range(1000, rangeEnd, 1000));
                        updateDisplayedData();
                      }}
                    >
                      <p
                        style={{ position: "relative", top: "-3.5em" }}
                      >{`Tranche ${bracket.bracket}`}</p>
                      <p
                        style={{
                          position: "relative",
                          top: "-3.5em",
                          fontSize: "1.5em",
                        }}
                      >{`${bracket.rate} %`}</p>
                    </Resizable>
                    {CSGBrackets.values[i] !== undefined && (
                      <>
                        <div>Seuil</div>
                        <InputNumber
                          min={0}
                          max={1000000}
                          defaultValue={CSGBrackets.values[i].floor}
                          value={CSGBrackets.values[i].floor}
                          formatter={(value) =>
                            `${value.toLocaleString("fr")} €`
                          }
                          onChange={(val) => {
                            let newBrackets = CSGBrackets.values;
                            newBrackets[i].floor = val;
                            setCSGBrackets({
                              ...CSGBrackets,
                              values: [...newBrackets],
                            });
                            updateDisplayedData();
                          }}
                        />
                      </>
                    )}
                  </div>
                </>
              ))}
              <div style={{ height: "20em" }}>
                <Button
                  type="primary"
                  shape="circle"
                  icon={<MinusOutlined />}
                  size="large"
                  style={{
                    top: "40%",
                    left: "2.2em",
                    border: "2px solid blue",
                    backgroundColor: "transparent",
                    color: "blue",
                  }}
                  onClick={() => {
                    let newBrackets = CSGBrackets.values;
                    newBrackets.pop();
                    setCSGBrackets({
                      ...CSGBrackets,
                      values: [...newBrackets],
                    });
                    if (autoRange) {
                      setRangeEnd(
                        (
                          CSGBrackets.values[CSGBrackets.values.length - 1]
                            .floor / 10000
                        ).toFixed(0) * 20000
                      );
                    }
                    setRevenues(range(1000, rangeEnd, 1000));
                    updateDisplayedData();
                  }}
                />
                <Button
                  type="primary"
                  shape="circle"
                  icon={<PlusOutlined />}
                  size="large"
                  style={{
                    top: "60%",
                    border: "2px solid blue",
                    backgroundColor: "transparent",
                    color: "blue",
                  }}
                  onClick={() => {
                    setCSGBrackets({
                      ...CSGBrackets,
                      values: [
                        ...CSGBrackets.values,
                        {
                          bracket: CSGBrackets.values.length + 1,
                          rate: (CSGBrackets.values[
                            CSGBrackets.values.length - 1
                          ].rate >= 90
                            ? 100
                            : CSGBrackets.values[CSGBrackets.values.length - 1]
                                .rate * 1.1
                          ).toFixed(0),
                          floor: (
                            CSGBrackets.values[CSGBrackets.values.length - 1]
                              .floor *
                              1.1 +
                            1000
                          ).toFixed(0),
                        },
                      ],
                    });
                    if (autoRange) {
                      setRangeEnd(
                        (
                          CSGBrackets.values[CSGBrackets.values.length - 1]
                            .floor / 10000
                        ).toFixed(0) * 20000
                      );
                    }
                    setRevenues(range(1000, rangeEnd, 1000));
                    updateDisplayedData();
                  }}
                />
              </div>
            </div>
          </Layout>
          <Layout style={{ padding: "1em" }}>
            <Row>
              <Col span={4}>
                <InputNumber
                  value={maxRate}
                  onChange={(val) => {
                    setMaxRate(val);
                    updateDisplayedData();
                  }}
                  min={1}
                  max={100}
                ></InputNumber>
                <Checkbox
                  checked={showIR}
                  onChange={(e) => {
                    setShowIR(e.target.checked);
                    updateDisplayedData();
                  }}
                >
                  IR
                </Checkbox>
                <Checkbox
                  checked={showCSG}
                  onChange={(e) => {
                    setShowCSG(e.target.checked);
                    updateDisplayedData();
                  }}
                >
                  CSG
                </Checkbox>
                <Checkbox
                  checked={showCRDS}
                  onChange={(e) => {
                    setShowCRDS(e.target.checked);
                    updateDisplayedData();
                  }}
                >
                  CRDS
                </Checkbox>
              </Col>
              <Col span={12}></Col>
              <Col
                span={2}
                style={{ height: "100%", textAlign: "right", padding: 0 }}
              >
                <h5 style={{ fontSize: "1em" }}>Revenu max</h5>
              </Col>
              <Col
                span={6}
                style={{
                  height: "100%",
                  textAlign: "left",
                  paddingLeft: "1em",
                }}
              >
                <Radio.Group
                  defaultValue={autoRange}
                  onChange={(e) => {
                    setAutoRange(e.target.value);
                    setRevenues(range(1000, rangeEnd, 1000));
                    updateDisplayedData();
                  }}
                >
                  <Radio.Button
                    type="primary"
                    value={true}
                    onClick={() => {
                      setRangeEnd(
                        (
                          IRBrackets[IRBrackets.length - 1].floor / 10000
                        ).toFixed(0) * 20000
                      );
                      setRevenues(range(1000, rangeEnd, 1000));
                      updateDisplayedData();
                    }}
                  >
                    Auto
                  </Radio.Button>
                  <Radio.Button type="primary" value={false}>
                    Fixe
                  </Radio.Button>
                </Radio.Group>
                <InputNumber
                  style={{ marginLeft: "1em", width: "10em" }}
                  disabled={autoRange}
                  value={rangeEnd}
                  formatter={(value) => `${value.toLocaleString("fr")} €`}
                  min={0}
                  max={1000000000000}
                  onChange={(val) => {
                    setRangeEnd(val);
                    setRevenues(range(1000, rangeEnd, 1000));
                    updateDisplayedData();
                  }}
                ></InputNumber>
              </Col>
            </Row>
            <Content
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              <ResponsiveContainer width="100%" height="100%">
                <AreaChart width={500} height={300} data={distributionData}>
                  <CartesianGrid strokeDasharray="10 10" />
                  <XAxis
                    dataKey="revenue"
                    domain={[0, 30000]}
                    ticks={range(10000, rangeEnd, 10000)}
                    tickFormatter={(string) => string / 1000 + "K"}
                    label={{
                      value: "",
                      position: "bottom",
                    }}
                  />
                  <YAxis
                    dataKey="distribution"
                    // domain={[0, 50000]}
                    label={{
                      value: "Taux d'imposition",
                      position: "left",
                      angle: -90,
                    }}
                  />
                  <Tooltip />
                  <Legend />
                  <Area
                    type="monotone"
                    dataKey="distribution"
                    stroke="green"
                    fill="green"
                  />
                </AreaChart>
              </ResponsiveContainer>
              <ResponsiveContainer width="100%" height="100%">
                <AreaChart width={500} height={300} data={distributionAccData}>
                  <CartesianGrid strokeDasharray="10 10" />
                  <XAxis
                    dataKey="revenue"
                    domain={[0, 30000]}
                    ticks={range(10000, rangeEnd, 10000)}
                    tickFormatter={(string) => string / 1000 + "K"}
                    label={{
                      value: "",
                      position: "bottom",
                    }}
                  />
                  <YAxis
                    dataKey="distribution"
                    domain={[0, 50000]}
                    label={{
                      value: "Taux d'imposition",
                      position: "left",
                      angle: -90,
                    }}
                  />
                  <Tooltip />
                  <Legend />
                  <Area
                    type="monotone"
                    dataKey="distribution"
                    stroke="green"
                    fill="green"
                  />
                </AreaChart>
              </ResponsiveContainer>
              <ResponsiveContainer width="100%" height="100%">
                {levyDistributionData && (
                  <AreaChart
                    width={500}
                    height={300}
                    data={levyDistributionData}
                  >
                    <CartesianGrid strokeDasharray="10 10" />
                    <XAxis
                      dataKey="revenue"
                      domain={[0, rangeEnd]}
                      ticks={range(10000, rangeEnd, 10000)}
                      tickFormatter={(string) => string / 1000 + "K"}
                      label={{
                        value: "",
                        position: "bottom",
                      }}
                    />
                    <YAxis
                      dataKey="distribution"
                      domain={[0, 1]}
                      label={{
                        value: "Taux d'imposition",
                        position: "left",
                        angle: -90,
                      }}
                    />
                    <Tooltip />
                    <Legend />
                    <Area
                      type="monotone"
                      dataKey="distribution"
                      stroke="green"
                      fill="green"
                    />
                  </AreaChart>
                )}
              </ResponsiveContainer>
              <ResponsiveContainer width="100%" height="150%">
                <AreaChart
                  width={500}
                  height={300}
                  data={displayedData}
                  margin={{
                    top: 0,
                    right: 30,
                    left: 20,
                    bottom: 5,
                  }}
                >
                  <CartesianGrid strokeDasharray="10 10" />
                  <XAxis
                    dataKey="revenue"
                    domain={[0, rangeEnd]}
                    ticks={range(10000, rangeEnd, 10000)}
                    tickFormatter={(string) => string / 1000 + "K"}
                    label={{
                      value: "",
                      position: "bottom",
                    }}
                  />
                  <YAxis
                    dataKey="IRRate"
                    domain={[0, maxRate]}
                    tickFormatter={(string) => string + " %"}
                    label={{
                      value: "Taux d'imposition",
                      position: "left",
                      angle: -90,
                    }}
                  />
                  <Tooltip />
                  <Legend />
                  <Area
                    stackId="1"
                    type="monotone"
                    dataKey="CRDSRate"
                    stroke="green"
                    fill="green"
                  />
                  <Area
                    stackId="1"
                    type="monotone"
                    dataKey="CSGRate"
                    stroke="blue"
                    fill="blue"
                  />
                  <Area
                    stackId="1"
                    type="monotone"
                    dataKey="IRRate"
                    stroke="purple"
                    fill="purple"
                  />
                  <Area
                    type="monotone"
                    data={currentTotalData}
                    dataKey="totalRate"
                    stroke="red"
                    fill="none"
                  />
                  {/* <Area
                    type="monotone"
                    dataKey="revenueAfterTax"
                    stroke="#00f0ff"
                    fill="#00f0ff"
                  />
                  <Area
                    type="monotone"
                    dataKey="revenue"
                    stroke="#ff00ff"
                    fill="#ff00ff"
                  /> */}
                </AreaChart>
              </ResponsiveContainer>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </div>
  );
}

export default App;
